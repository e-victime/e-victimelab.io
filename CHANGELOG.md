# CHANGELOG

<!--- next entry here -->

## 1.0.3
2020-05-28

### Fixes

- **ci:** Fix CI error (29b94c9464e0c6fe4f976a94b54bf814d72a7357)

## 1.0.2
2020-05-28

### Fixes

- **ci:** Repair download links and versioning (deda9e31ea0c55594d6ff2a5589619960cc819bd)
- **ci:** Débug ci (88bf284eba63800e5f9bbc0b940fa94271f31889)
- **tech:** Add Electron and TypeScript (e4ef2c93bf4f9e9146f66c34a450c17ec8d789ee)
- **words:** Add Glossary (88d10a92b9c86f11ab3545361d7c89e518acf760)
- **team:** Add team members roles (04427ee925d62b1aafa9c89ae0b1e8a6889b8647)
- **ci:** Try repair ci (f69193cfcf6929377fe614bc52cdddeda03f7ce8)
- **title:** Change subtitle (cb1d25ee6bce2a00926e0415c8183a906b41ab13)
- **ci:** Fix var (102c84e27db5b19827d96140e379ad418de600e3)
- **pdf:** Link to releases (93e8371d5a6cf1a6925b800b9811498b24fe2db8)
- **pdf:** Link to releases (2493b37c7902ba710e0cb9560c71c36d78c66c48)
- **build:** Delete glossaire.adoc (31817c872c226f894786ca0778521b751525f7b0)

## 1.0.1
2020-05-28

### Fixes

- **ci:** Repair download links and versioning (cada794fb28d79d7f1a8b6851b392b53babcbd45)

## 1.0.0
2020-05-28

### Features

- **api:** Add swagger-ui doc (d5976f89d3caa2e0067ae2824818558f0b9e6927)
- **docs:** Generate PDF (ca0f41641e6d1a4f56ef2848b959932047360ec1)
- **bundle:** Define install path (bb72f838280cba245ac31c0e1a9491704763d9e8)

### Fixes

- **idea:** Add idea files (cb00b48c8237b725ff59629d1a63d30cc4b7e915)
- **idea:** Add idea files (fdc61c9788e561086e0a47dd06c45fe35b6613c9)
- **swagger:** Add file to use swagger un kubernetes (30a03437b2776554bf69be1f264c11811cbd149a)
- **bdd:** Add mcd (c0717b8c2f90abb761f2d61b6d49cc7924fa6b84)
- **api:** Add api doc (a96ae10b3e2754f88cf052066cead328016f645c)
- **planner:** Add planner img in doc (39900e9e9151890a23385ee2fde60d9201085c06)
- **api:** Add API documentation link (93f53f2ac7ba8b4e15cbc93d47eddfea9fb39d58)
- **bdd:** Change alt name (5a89a5f3ba40f10124b02cdfd237818db181fa91)
- **client:** Add client documentation (5e09ef15228af78e1225266dcaf0d158e2608487)
- **appendix:** Add techno in appendix (e213ad946457368407a6e3448ea613f9793247e4)
- **fonctionnement:** API before BDD (915cbc9a17d3e69c0d6b5178f807842ffdf8fd29)
- **url:** Fix ci/cd url (e8cc942df55a13f605077cc3742b009c6dc9a4f5)
- **ignore:** Ignore pdf file (360963cb776938c2e5cbc1f2530f9866d320e287)
- **scripts:** Change all script (6b7af05a5d3559118cef91284e5e4de946ffd4b6)
- **index:** Add appendix-tech (c2fe95f573971ea509170b4eb148fe07377d90e7)
- **index:** Move intro file (56b733e0517ab6c6d13d1486fc220855dcd826f8)
- **appendix:** Add appendix & clean repo (7f9a7997d587a2a360db4d00dd9622a5c3c17ade)
- **discovery:** Change zone URL (8154eaa29134912926065c28836eb7619a0e26fb)
- **doc:** Finish documentation documentation (a85d29b2b588b609a676bb6727ec36ef945d5216)
- **ignore:** Ignore build files but keep empty folder (71e4daad3348b3e89f2adebc858b7f6b04e4a7c5)
- **controller:** deprecate old search method (ebfe5957bef163c2f1e2faa176cf1c35d11032ff)
- **api:** Ajout de la documentation de spring search (6d1ae0d588915c47799150d62188d5ea62c1a725)
- **intro:** Ajout de la version et du lien vers le fichier PDF (8be7855c8503e6f25f92aba88518bd85fe4c96fd)
- **ci:** Ajout du versionning (b1d6e7455ff9ea199113860bebe89276dc35f3ee)
- **ci:** Fix closing quote (7bcb29783c674cffe831e9d5354880bc7d9dc0f0)
- **equipe:** Add authors names (be5200b49aa414b615325dc7855bb73c9e54cd79)
- **ci:** fix dependencies (0e242626a22a089f1a4dd0ef79c1c1268c523265)
- **ci:** Add bundle (98c1cc70b4fbe1c22bb6130781da3c12696ce372)