#!/usr/bin/env ruby

require_relative 'rb/asciidoc'

class Parser
  def self.parse(args)

    cliargs = {
        backend: 'html5',
        dir: 'public'
    }

    opts = OptionParser.new do |opts|
      opts.banner = "Utilisation: asciidoctor [options]"
      opts.on( '-h', '--help', 'Menu d\'aide' ) do
        puts opts
        exit
      end

      opts.on( '-b', '--backend BACKEND', String, 'Choix du backend : html5/pdf par défaut html5 est utilisé' ) do |backend|
        cliargs[:backend] = backend
      end

      opts.on( '-f', '--folder FOLDER', String, 'Choix du dossier de destination, public par défaut') do |dir|
        cliargs[:dir] = dir
      end

    end

    opts.parse(args)

    cliargs

  end
end

options = Parser.parse(ARGV)

include Asciidoc

Asciidoc::build(options[:backend],options[:dir])

