[#victime]
==== Victimes

Voici les différentes méthodes d’installation pour le micro-service de gestion des Victimes.

[#victime_lancement_locale]
===== Lancement locale

Pour lancer ce micro-service en local il faut dans un premier temps le compiler puis lancer l’exécution du programme.

NOTE: Ces étapes sont à réaliser dans le dossier du micro-service.

[source,bash]
----
$ ./mvnw package
$ java -jar /target/victime.jar
----

Le micro-service est maintenant accessible depuis link:http://localhost:8282[].

[#victime_conteneurisation]
===== Conteneurisation

Pour la génération du conteneur en CI/CD nous utilisons ce fichier `Dockerfile`.

[source,docker]
----
include::https://gitlab.com/e-victime/backend/victime/-/raw/master/Dockerfile[Dockerfile CI/CD]
----

IMPORTANT: Afin de pouvoir établir la liaison avec la base de données il faut adapter le fichier `src/main/resources/application-prod.properties`.

IMPORTANT: Afin de pouvoir établir la liaison avec le service de découverte il peut être nécessaire d'adapter les champs `defaultZone` et `hostname` du fichier `src/main/resources/bootstrap.yaml`.

[#victime_dployment_en_cicd]
===== Déploiement en CI/CD

En production nous utilisons la CI/CD gitlab avec la configuration ci-dessous. En plus de déployer notre solution elle effectue un versionnement automatique du projet.

[source,yaml]
----
include::https://gitlab.com/e-victime/backend/victime/-/raw/master/.gitlab-ci.yml[CI/CD]
----

NOTE: Les autres fichiers utilisés lors de l’exécution sont disponibles à la racine du dépôt : link:https://gitlab.com/e-victime/backend/victime/-/tree/master[]