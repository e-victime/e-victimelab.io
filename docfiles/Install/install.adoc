[#_mise_en_place]
:chapter: Mise_en_place
== Mise en place

Cette partie documente les étapes de mise en place des différents micro-services présents dans ce projet.

[#_backend]
=== Backend

Chaque service backend est dans le cadre de notre projet déployée sur un cluster Kubernetes. +
Pour notre déployment en production nous utilisons la CI/CD Gitlab afin de compiler, mettre dans un container puis déployer l'image sur Gitlab.

IMPORTANT: Les étapes de réalisation de cette documentation sont application à un système Linux et ne sont pas testées pour les systèmes Windows,
des différences mineurs peuvent être présentes notamment pour les installations en local.

==== Prérequis

.Lancement locale
* Java 8 + variable `JAVA_HOME` définie
* Internet

.Installation dans un conteneur
* Docker / Podman _(non testé)_
* Internet

.Dans tous les cas
* PostgreSQL

include::Personnel.adoc[]

include::Poste.adoc[]

include::Victime.adoc[]

include::Discovery.adoc[]

include::Gateway.adoc[]

include::Admin.adoc[]

[#_clients]
=== Clients

include::Client.adoc[]

//include::Web.adoc[]
